import pandas as pd
import numpy as np
import sklearn
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.manifold import TSNE


# Step 1: Download the data
print('Begin downloading data\n')
dataframe_all = pd.read_csv('https://d396qusza40orc.cloudfront.net/predmachlearn/pml-training.csv')
num_rows = dataframe_all.shape[0]


# Step 2: Clean the data
# Count the number of missing elements (NaN) in each column
print('Begin cleaning data\n')
counter_nan = dataframe_all.isnull().sum()
counter_without_nan = counter_nan[counter_nan == 0]
# Remove the columns with missing elements
dataframe_all = dataframe_all[counter_without_nan.keys()]
# Remove the first 7 columns which contain no discriminative information
dataframe_all = dataframe_all.ix[:, 7:]


# Step 3: Create feature vectors
print('Begin creating feature vectors\n')
x = dataframe_all.ix[:, :-1].values
standard_scalar = StandardScaler()
x_std = standard_scalar.fit_transform(x)


# t distributed stochastic neighbor embedding (t-SNE) visualisation
print('Start TSNE\n')
tsne = TSNE(n_components=2, random_state=0)
x_test_2d = tsne.fit_transform(x_std)


# Scatter plot the sample points among 5 classes
print('Plot time!!!\n')
markers = ('s', 'd', 'o', '^', 'v')
color_map = {0:'red', 1:'blue', 2:'lightgreen', 3:'purple', 4:'cyan'}
plt.figure()
for idx, cl in enumerate(np.unique(x_test_2d)):
    plt.scatter(x=x_test_2d[int(cl), 0], y=x_test_2d[int(cl), 1])

plt.show()